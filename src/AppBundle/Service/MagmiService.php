<?php

namespace AppBundle\Service;

use AppBundle\Entity\Article;
use AppBundle\Entity\Log;
use AppBundle\Entity\Magmi;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MagmiService
 * @package AppBundle\Service
 */
class MagmiService
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $em;

    private $sizePath,$colorPath;
    private $sizeData,$colorData;

    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;

        $this->sizePath = $this->container->get('kernel')->getRootDir() . "/../src/AppBundle/Resources/csv/asics_sizes.csv";
        $this->colorPath = $this->container->get('kernel')->getRootDir() . "/../src/AppBundle/Resources/csv/asics_colors.csv";

        $this->sizeData = $this->parseSizeCSV($this->sizePath);
        $this->colorData = $this->parseCSV($this->colorPath);
    }

    /**
     * Importe les fichiers csv passé en paramètre en BD
     * @param $filesPath
     * @return int
     */
    public function import($filesPath)
    {
        $compt = 0;
        foreach ($filesPath as $path){
            $log = new Log();
            $data = $this->parseCSV($path);
            foreach ($data as $product) {
                $article = new Article();
                $article->setLog($log);
                $size_ranges = $this->parseSizeRanges($product["Size ranges"]);
                $size_ranges[] = null;
                //Pour chaque produit créer autant de "simple" que de taille disponible dans size_ranges
                $skus = [];
                foreach ($size_ranges as $key => $size){
                    $magmi = new Magmi();
                    $sku = "MAG" . (sprintf("%06d", $key+1));
                    $skus[] = $sku;
                    $magmi->setSku($sku);
                    if($size == null) {
                        $magmi->setType("configurable");
                        $magmi->setSimplesSkus(implode(",", $skus));
                        $magmi->setConfigurableAttributes("size");
                    }else{
                        $magmi->setType("simple");
                        $magmi->setSize($this->assignSize($product["age_group"], $product["gender"], $size));
                    }
                    $magmi->setManufacturer($product["manufacturer"]);
                    $magmi->setManufacturerSku($product["manufacturer_sku"]);
                    $magmi->setName($product["name"]);
                    $magmi->setBrand($product["brand"]);
                    $magmi->setColor($this->assignColor($product['color']));
                    $magmi->setGender($product["gender"]);
                    $magmi->setAgeGroup($product["age_group"]);
                    $magmi->setDescription($product["description"]);
                    $magmi->setPrice($product["price"]);
                    $magmi->setMsrp($product["msrp"]);

                    $magmi->setArticle($article);
                    $this->em->persist($magmi);
                    $compt++;
                }
                $log->setCreatedAt(new \DateTime("now"));
                $this->em->persist($log);
            }
            $this->em->flush();
        }
        return $compt;
    }

    /**
     * Exporte en csv les variantes d'un article
     * @param Article $article
     * @return string
     */
    public function exportCSV(Article $article)
    {
        /** @var Magmi[] $magmis */
        $magmis = $article->getMagmis();
        $csv = implode(";", [
            "type","sku","manufacturer","manufacturer_sku","name","brand","color","size","gender","age_group","description","price","msrp","configurable_attributes","simples_skus","\n"
        ]);

        foreach ($magmis as $magmi){
            $csv .= implode(";",
                [
                    $magmi->getType(),
                    $magmi->getSku(),
                    $magmi->getManufacturer(),
                    $magmi->getManufacturerSku(),
                    $magmi->getName(),
                    $magmi->getBrand(),
                    $magmi->getColor(),
                    $magmi->getSize(),
                    $magmi->getGender(),
                    $magmi->getAgeGroup(),
                    utf8_decode(trim(preg_replace('/\s+|\r+|\n+|\t+/', ' ', $magmi->getDescription()))),
                    $magmi->getPrice(),
                    $magmi->getMsrp(),
                    $magmi->getConfigurableAttributes(),
                    $magmi->getSimplesSkus() . "\n",
                ]);
        }
        return $csv;
    }

    /**
     * Affecte la chaine de caractère de couleur (traduction)
     * @param $colorToTranslate
     * @return string
     */
    private function assignColor($colorToTranslate)
    {
        $colors = preg_split("/[\/]+/", $colorToTranslate,-1);
        $color_str = "";
        foreach ($this->colorData as $key => $colorData_row) {
            foreach ($colors as $color) {
                if($colorData_row['Désignation fournisseur'] === $color){
                    if($color_str !== "") $color_str .= "/";
                    $color_str .= $colorData_row['Désignation commune'];
                }
            }
        }
        return $color_str;
    }

    /**
     * Affecte une taille en fonction du genre et de l'age conseillé du produit
     * @param $age_group
     * @param $gender
     * @param $US_size
     * @return string
     */
    private function assignSize($age_group, $gender, $US_size)
    {
        $EU_size = "";
        $US_size = strval($US_size);
        if($age_group === "Adulte"){
            if($gender === "Homme"){
                if(array_key_exists($US_size,$this->sizeData["HOMME"])){
                    $EU_size = $this->sizeData["HOMME"][$US_size];
                }
            }elseif ($gender === "Femme"){
                if(array_key_exists($US_size,$this->sizeData["FEMME"])){
                    $EU_size = $this->sizeData["FEMME"][$US_size];
                }
            }
        }elseif($age_group === "Kid" || $age_group === "Junior"){
            if(array_key_exists($US_size,$this->sizeData["ENFANTS"])){
                $EU_size = $this->sizeData["ENFANTS"][$US_size];
            }
        }
        return $EU_size;
    }

    /**
     * Parse les tailles, retourne un tableau contenant chaque taille à enregistrer dans le magmi
     * @param $size_ranges
     * @return array
     */
    private function parseSizeRanges($size_ranges)
    {
        $sizesRanges = [];
        $sizes = preg_split("/,/", $size_ranges);
        foreach ($sizes as $size){
            if(preg_match('/\d+\-\d+/',$size)){
                //taille 1-7 par exemple
                $range = preg_split("/\-/", $size);
                $sizesRanges = array_merge($sizesRanges,range($range[0],$range[1],0.5));
            }else{
                $sizesRanges[] = $size;
            }
        }
        return $sizesRanges;
    }

    /**
     * Parse un fichier CSV conforme
     * @param $path
     * @return array
     */
    private function parseCSV($path)
    {
        $header = null;
        $data = [];
        if($handle = fopen($path, "r")){
            while(($row = fgetcsv($handle)) !== false){
                if(!$header){
                    $header = $row;
                }else{
                    $data[] = array_combine($header, $row);
                }
            }
        }
        return $data;
    }

    /**
     * Retourne les tailles classées par catégorie (Homme,femme,...) puis par taille (US et EU)
     * @param $path
     * @return array
     */
    private function parseSizeCSV($path)
    {
        $data = [];

        if($handle = fopen($path, "r")){
            $header = null;
            $line = 0;
            $US = [];
            $EU = [];
            while(($row = fgetcsv($handle, 0, ',')) !== false){
                //non ligne vide
                if($row[0] !== "" && ($row[1] === "" || $row[1] !== "")){
                    $line++;
                    //HOMME, FEMME ou ENFANT
                    if($row[0] !== "" && $row[1] === ""){
                        $data[$row[0]] = [];
                        $header = $row[0];
                    }
                    //Taille US ou EU
                    elseif($row[0] === "US" || $row[0] === "EU"){
                        foreach ($row as $size){
                            if($size != ""){
                                if($row[0] === "US") $US[] = $size;
                                elseif($row[0] === "EU") $EU[] = $size;
                            }
                        }
                        if($line % 3 == 0){
                            //Remplissage des données dans le tableau
                            $data[$header] = array_combine($US,$EU);
                            //Remise à zéro des buffers
                            $US = []; $EU = [];
                        }
                    }
                }
            }
        }
        return $data;
    }

}