<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Log;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class LogController extends Controller
{
    /**
     * Liste des logs
     * @return Response
     */
    public function indexAction()
    {
        $logs = $this->getDoctrine()->getManager()->getRepository("AppBundle:Log")->findAll();

        return $this->render("AppBundle:Log:index.html.twig", [
            'logs' => $logs
        ]);
    }

    /**
     * Afficher le contenu du log
     * @param Log $log
     * @return Response
     */
    public function showAction(Log $log)
    {
        return $this->render("AppBundle:Log:show.html.twig", [
            'log' => $log
        ]);
    }



}