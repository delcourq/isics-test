<?php

namespace AppBundle\Controller;



use AppBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends Controller
{

    /**
     * Afficher le contenu de l'article (les différentes variantes)
     * @param Article $article
     * @return Response
     */
    public function showAction(Article $article)
    {
        return $this->render("AppBundle:Article:show.html.twig", [
           'article' => $article,
        ]);
    }

    /**
     * @param Article $article
     * @return Response
     */
    public function exportCsvAction(Article $article)
    {
        $csv = $this->get("app.magmi")->exportCSV($article);

        $response = new Response();
        $response->setContent($csv);
        $response->headers->set("Content-Type", "text/csv");
        $response->headers->set("Content-Disposition", "attachment; filename=\"" . str_replace(" ","_", $article->getMagmis()[0]->getName()) . ".csv\"");

        return $response;
    }

}