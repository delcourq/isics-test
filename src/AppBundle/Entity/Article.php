<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity()
 */
class Article
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Log
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Log", inversedBy="articles")
     */
    private $log;

    /**
     * @var Magmi[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Magmi", cascade={"persist", "remove"}, mappedBy="article"))
     */
    private $magmis;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->magmis = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set log
     *
     * @param \AppBundle\Entity\Log $log
     *
     * @return Article
     */
    public function setLog(\AppBundle\Entity\Log $log = null)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * Get log
     *
     * @return \AppBundle\Entity\Log
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Add magmi
     *
     * @param \AppBundle\Entity\Magmi $magmi
     *
     * @return Article
     */
    public function addMagmi(\AppBundle\Entity\Magmi $magmi)
    {
        $this->magmis[] = $magmi;

        return $this;
    }

    /**
     * Remove magmi
     *
     * @param \AppBundle\Entity\Magmi $magmi
     */
    public function removeMagmi(\AppBundle\Entity\Magmi $magmi)
    {
        $this->magmis->removeElement($magmi);
    }

    /**
     * Get magmis
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMagmis()
    {
        return $this->magmis;
    }
}
