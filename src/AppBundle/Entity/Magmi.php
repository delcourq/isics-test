<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Magmi
 *
 * @ORM\Table(name="magmi")
 * @ORM\Entity()
 */
class Magmi
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=255)
     */
    private $sku;

    /**
     * @var string
     * @ORM\Column(name="manufacturer", type="string", length=255)
     */
    private $manufacturer;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer_sku", type="string", length=255)
     */
    private $manufacturerSku;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="brand", type="string", length=255)
     */
    private $brand;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=255, nullable=true)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=255)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="age_group", type="string", length=255)
     */
    private $ageGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="msrp", type="decimal", precision=10, scale=2)
     */
    private $msrp;

    /**
     * @var string
     *
     * @ORM\Column(name="configurable_attributes", type="string", length=255, nullable=true)
     */
    private $configurableAttributes;

    /**
     * @var string
     *
     * @ORM\Column(name="simples_skus", type="string", length=255, nullable=true)
     */
    private $simplesSkus;

    /**
     * @var Article
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Article", cascade={"persist", "remove"}, inversedBy="magmis")
     */
    private $article;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Magmi
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set sku
     *
     * @param string $sku
     *
     * @return Magmi
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set manufacturerSku
     *
     * @param string $manufacturerSku
     *
     * @return Magmi
     */
    public function setManufacturerSku($manufacturerSku)
    {
        $this->manufacturerSku = $manufacturerSku;

        return $this;
    }

    /**
     * Get manufacturerSku
     *
     * @return string
     */
    public function getManufacturerSku()
    {
        return $this->manufacturerSku;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Magmi
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set brand
     *
     * @param string $brand
     *
     * @return Magmi
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Magmi
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set size
     *
     * @param string $size
     *
     * @return Magmi
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Magmi
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set ageGroup
     *
     * @param string $ageGroup
     *
     * @return Magmi
     */
    public function setAgeGroup($ageGroup)
    {
        $this->ageGroup = $ageGroup;

        return $this;
    }

    /**
     * Get ageGroup
     *
     * @return string
     */
    public function getAgeGroup()
    {
        return $this->ageGroup;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Magmi
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Magmi
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set msrp
     *
     * @param string $msrp
     *
     * @return Magmi
     */
    public function setMsrp($msrp)
    {
        $this->msrp = $msrp;

        return $this;
    }

    /**
     * Get msrp
     *
     * @return string
     */
    public function getMsrp()
    {
        return $this->msrp;
    }

    /**
     * Set configurableAttributes
     *
     * @param string $configurableAttributes
     *
     * @return Magmi
     */
    public function setConfigurableAttributes($configurableAttributes)
    {
        $this->configurableAttributes = $configurableAttributes;

        return $this;
    }

    /**
     * Get configurableAttributes
     *
     * @return string
     */
    public function getConfigurableAttributes()
    {
        return $this->configurableAttributes;
    }

    /**
     * Set simplesSkus
     *
     * @param string $simplesSkus
     *
     * @return Magmi
     */
    public function setSimplesSkus($simplesSkus)
    {
        $this->simplesSkus = $simplesSkus;

        return $this;
    }

    /**
     * Get simplesSkus
     *
     * @return string
     */
    public function getSimplesSkus()
    {
        return $this->simplesSkus;
    }

    /**
     * Set article
     *
     * @param \AppBundle\Entity\Article $article
     *
     * @return Magmi
     */
    public function setArticle(\AppBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \AppBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     *
     * @return Magmi
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }
}
