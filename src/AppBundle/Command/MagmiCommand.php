<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MagmiCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('magmi:import')
            ->setDescription('...')
            ->addArgument('paths',  InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'Argument description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $paths = $input->getArgument('paths');
        $nbInserted = $this->getContainer()->get('app.magmi')->import($paths);
        $output->writeln($nbInserted . " lignes ont été insérées");
    }



}


