isics-test
==========

Symfony version : 3.2.8

============== Description ==============

Test d'entrée (stage) dans la société Isics.
Créer un programme qui importe les données (articles) d'un fichier "commerçant" dans la base
afin de les utiliser dans un site de e-commerce.


============== Fonctionnalités ==============

- Importer les variantes des articles d'un (ou plusieurs ) fichier .csv avec une commande console pour un site de e-commerce
- Importation des données dans la base avec un système de log
- Exporter en .csv les variantes d'un seul article

============== Commande console =============
   
Commande :
   
    php bin/console magmi:import chemin_du_fichier1.csv [chemin_du_fichier2.csv ...]
   
Exemple :
     
    php bin/console magmi:import src/AppBundle/Resources/csv/asics1.csv src/AppBundle/Resources/csv/asics2.csv src/AppBundle/Resources/csv/asics3.csv
